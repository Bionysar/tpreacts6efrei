export const addArticle = newArticle => {
  return {
    type: "ADD_ARTICLE",
    newArticle
  };
};
