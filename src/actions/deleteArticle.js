export const deleteArticle = id => {
  return {
    type: "DELETE_ARTICLE",
    id
  };
};
