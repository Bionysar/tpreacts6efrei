export const keepShoppingCart = shoppingCart => {
  return {
    type: "KEEP_ITEMS",
    shoppingCart
  };
};
