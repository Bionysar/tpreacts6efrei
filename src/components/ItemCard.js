import React, { Component } from "react";
import "../css/ItemCard.css";
import "bootstrap/dist/css/bootstrap.css";
import { Nav } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";

class ItemCard extends Component {
  render() {
    return (
      <div className="card-steven">
        <img
          src={this.props.picture}
          className="redim_img"
          alt={this.props.alt}
        />
        <div className="card-body">
          <h5 className="card-title">{this.props.name}</h5>
          <h5 className="card-title">{this.props.price} €</h5>
          <p className="card-text">{this.props.brand}</p>
          <div className="button-container">
            <LinkContainer
              className="btn btn-primary"
              to={"../ArticlePage/" + this.props.id}
            >
              <Nav.Link>Voir plus</Nav.Link>
            </LinkContainer>
            <a
              href="#"
              className="btn btn-primary"
              onClick={() =>
                this.props.onCartClick({
                  id: this.props.id,
                  picture: this.props.picture,
                  name: this.props.name,
                  quantity: 1,
                  price: this.props.price,
                  alt: this.props.alt
                })
              }
            >
              Mettre dans le panier
            </a>
          </div>
        </div>
      </div>
    );
  }
}

export default ItemCard;
