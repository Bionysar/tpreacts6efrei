import React, { Component } from "react";
import "../css/LayoutItems.css";
import "bootstrap/dist/css/bootstrap.css";
import NavBarCat from "./NavBarCat";
import ItemCard from "./ItemCard";
import { connect } from "react-redux";
import { addArticle } from "../actions/addArticle";

class LayoutItems extends Component {
  constructor(props) {
    super(props);
    this.state = {
      brand: [],
      selectedBrand: ""
    };
  }
  getData = () => {
    fetch("http://localhost:9000/brand")
      .then(res => res.json())
      .then(res => this.setState({ brand: res }))
      .catch(error => console.error("Error:", error));
  };

  componentWillMount() {
    this.getData();
  }
  handleCartClick = newArticle => {
    this.props.addArticle(newArticle);
  };

  handleOptionChange = changeEvent => {
    this.setState({
      selectedBrand: changeEvent.target.value
    });
  };

  withoutFilter = () => {
    if (this.props.match.params.category === "ALL") {
      return this.props.dataBasedItems.map((element, index) => (
        <ItemCard
          id={element.id}
          picture={element.picture}
          name={element.name}
          price={element.price}
          brand={element.brand}
          index={index}
          onCartClick={this.handleCartClick}
          alt={element.alt}
          key={index}
        />
      ));
    } else {
      return this.props.dataBasedItems
        .filter(
          element =>
            element.category.toUpperCase() ===
            this.props.match.params.category.toUpperCase()
        )
        .map((element, index) => (
          <ItemCard
            id={element.id}
            picture={element.picture}
            name={element.name}
            price={element.price}
            brand={element.brand}
            index={index}
            onCartClick={this.handleCartClick}
            alt={element.alt}
            key={index}
          />
        ));
    }
  };

  withFilter = () => {
    if (this.props.match.params.category === "ALL") {
      return this.props.dataBasedItems
        .filter(
          element =>
            element.brand.toUpperCase() ===
            this.state.selectedBrand.toUpperCase()
        )
        .map((element, index) => (
          <ItemCard
            id={element.id}
            picture={element.picture}
            name={element.name}
            price={element.price}
            brand={element.brand}
            index={index}
            onCartClick={this.handleCartClick}
            alt={element.alt}
            key={index}
          />
        ));
    } else {
      return this.props.dataBasedItems
        .filter(
          element =>
            element.category.toUpperCase() ===
            this.props.match.params.category.toUpperCase()
        )
        .filter(
          element =>
            element.brand.toUpperCase() ===
            this.state.selectedBrand.toUpperCase()
        )
        .map((element, index) => (
          <ItemCard
            id={element.id}
            picture={element.picture}
            name={element.name}
            price={element.price}
            brand={element.brand}
            index={index}
            onCartClick={this.handleCartClick}
            alt={element.alt}
            key={index}
          />
        ));
    }
  };

  render() {
    return (
      <div className="vente">
        <NavBarCat />

        <div className="wrapping-page">
          <div className="sideLeft">
            <div className="SideBarre">
              <h2> Marque</h2>
              <form>
                <div className="form-check">
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="exampleRadios"
                      value=""
                      checked={this.state.selectedBrand === ""}
                      onChange={this.handleOptionChange}
                    />
                    <label className="form-check-label">Aucune marque</label>
                  </div>
                </div>
                {this.state.brand.map((element, index) => (
                  <div className="form-check" key={index}>
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        type="radio"
                        name="exampleRadios"
                        value={element.brand}
                        checked={this.state.selectedBrand === element.brand}
                        onChange={this.handleOptionChange}
                      />
                      <label className="form-check-label">
                        {element.brand}
                      </label>
                    </div>
                  </div>
                ))}
              </form>
            </div>
          </div>
          <div className="card-group-steven">
            {this.state.selectedBrand !== ""
              ? this.withFilter()
              : this.withoutFilter()}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    shoppingCartElements: state.shoppingCartElements,
    dataBasedItems: state.dataBasedItems
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addArticle: newArticle => {
      dispatch(addArticle(newArticle));
    }
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LayoutItems);
