import React, { Component } from "react";
import "../css/NavBarCat.css";
import { Navbar, Nav } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import { LinkContainer } from "react-router-bootstrap";

class NavBarCat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: []
    };
  }
  getData = () => {
    fetch("http://localhost:9000/categories")
      .then(res => res.json())
      .then(res => this.setState({ categories: res }))
      .catch(error => console.error("Error:", error));
  };

  componentWillMount() {
    this.getData();
  }
  render() {
    return (
      <div className="Sells">
        <Navbar bg="light" variant="light">
          <Navbar.Brand>Catégories</Navbar.Brand>
          <Nav className="mr-auto">
            <LinkContainer to={"../layoutItems/ALL"}>
              <Nav.Link> ALL</Nav.Link>
            </LinkContainer>
            {this.state.categories.map((element, index) => (
              <LinkContainer
                to={"../layoutItems/" + element.category}
                key={index}
              >
                <Nav.Link>{element.category}</Nav.Link>
              </LinkContainer>
            ))}
          </Nav>
        </Navbar>
      </div>
    );
  }
}

export default NavBarCat;
