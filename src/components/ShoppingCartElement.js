import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.css";
import "../css/ShoppingCartElement.css";
import { connect } from "react-redux";
import Cookies from "universal-cookie";

class ShoppingCartElement extends Component {
  constructor(props) {
    super(props);

    this.state = {
      quantity: this.props.quantity
    };
  }

  handleChange = event => {
    this.setState({
      quantity: event.target.value
    });
  };

  render() {
    const cookies = new Cookies();
    const panier = cookies.get("panier");
    return (
      <tr>
        <td>
          <img
            className="img-size"
            src={this.props.picture}
            alt={this.props.alt}
          />{" "}
        </td>
        <td>{this.props.name}</td>
        <td>
          In stock ({""}
          {this.props.dataBasedItems.length > 0 &&
            this.props.dataBasedItems.filter(
              element => element.id.toString() === this.props.id.toString()
            )[0].quantity}
          )
        </td>
        <td>
          <input
            className="form-control"
            type="text"
            value={this.state.quantity}
            onChange={this.handleChange}
            style={{ textAlign: "center" }}
            readOnly
          />
        </td>
        <td className="text-right">{this.props.price}€</td>
        <td className="text-right">
          <button
            className="btn btn-sm btn-danger"
            onClick={() => this.props.onClick(this.props.id)}
          >
            <i className="fa fa-trash" />{" "}
          </button>{" "}
        </td>
      </tr>
    );
  }
}

const mapStateToProps = state => {
  return {
    dataBasedItems: state.dataBasedItems
  };
};

export default connect(mapStateToProps)(ShoppingCartElement);
