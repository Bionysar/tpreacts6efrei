import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./pages/App.js";
import { BrowserRouter as Router } from "react-router-dom";
import { createStore } from "redux";
import { Provider } from "react-redux";
import rootReducer from "./reducers/rootReducer";
import Cookies from "universal-cookie";

const store = createStore(rootReducer);
const cookies = new Cookies();
if (cookies.get("panier") === undefined) {
  cookies.set("panier", [], { path: "/" });
}

ReactDOM.render(
  <Router>
    <Provider store={store}>
      <App />
    </Provider>
  </Router>,
  document.getElementById("root")
);
