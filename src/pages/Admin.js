import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.css";
import "../css/Admin.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserShield, faPlus } from "@fortawesome/free-solid-svg-icons";
import Cookies from "universal-cookie";

class Admin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      stats: [],
      users: [],
      stock: []
    };
  }
  getData = () => {
    fetch("http://localhost:9000/admin/stat")
      .then(res => res.json())
      .then(res => this.setState({ stats: res }))
      .catch(error => console.error("Error:", error));
    fetch("http://localhost:9000/admin/user")
      .then(res => res.json())
      .then(res => this.setState({ users: res }))
      .catch(error => console.error("Error:", error));
    fetch("http://localhost:9000/admin/stock")
      .then(res => res.json())
      .then(res => this.setState({ stock: res }))
      .catch(error => console.error("Error:", error));
  };

  componentWillMount() {
    this.getData();
  }

  updateUser = (userID, userTypeID) => {
    fetch("http://localhost:9000/admin/changeop", {
      method: "POST",
      body: JSON.stringify({
        Data: { userID: userID, userTypeID: userTypeID }
      }),
      headers: {
        "Content-Type": "application/json"
      }
    }).catch(error => console.error("Error:", error));
    document.location.reload(true);
  };

  addStock = (quantity, productID) => {
    fetch("http://localhost:9000/admin/addstock", {
      method: "POST",
      body: JSON.stringify({
        Data: { quantity: quantity, productID: productID }
      }),
      headers: {
        "Content-Type": "application/json"
      }
    }).catch(error => console.error("Error:", error));
    document.location.reload(true);
  };

  componentDidMount() {
    const cookies = new Cookies();
    if (cookies.get("User") === undefined) {
      this.props.history.push("/");
    }
  }

  render() {
    return (
      <div>
        <h1>STATS COMMANDES</h1>
        <div className="row justify-content-md-center">
          <table className="table col col-lg-6 table-striped">
            <thead className="thead-dark">
              <tr>
                <th scope="col">Id commmande</th>
                <th scope="col">Quantité d'articles</th>
              </tr>
            </thead>
            <tbody>
              {this.state.stats.map((element, index) => (
                <tr key={index}>
                  <td>{element.billid}</td>
                  <td>{element.quantity}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <h1>UTILISATEURS</h1>
        <div className="row justify-content-md-center">
          <table className="table col col-lg-6 table-striped">
            <thead className="thead-dark">
              <tr>
                <th scope="col">Email</th>
                <th scope="col">Nom</th>
                <th scope="col">Prenom</th>
                <th scope="col">Type</th>
                <th scope="col">Switch Admin/Utilisateur</th>
              </tr>
            </thead>
            <tbody>
              {this.state.users.map((element, index) => (
                <tr key={index}>
                  <td>{element.mail}</td>
                  <td>{element.nom}</td>
                  <td>{element.prenom}</td>
                  <td>{element.type === 1 ? "admin" : "utilisateur"}</td>
                  <td>
                    <button
                      className="shield"
                      onClick={event => {
                        this.updateUser(element.id, element.type);
                      }}
                    >
                      <FontAwesomeIcon icon={faUserShield} />
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <h1>STOCK</h1>
        <div className="row justify-content-md-center">
          <table className="table col col-lg-6 table-striped">
            <thead className="thead-dark">
              <tr>
                <th scope="col">Nom</th>
                <th scope="col">Quantité</th>
                <th scope="col">Augmenter Quantité</th>
              </tr>
            </thead>
            <tbody>
              {this.state.stock.map((element, index) => (
                <tr key={index}>
                  <td>{element.name}</td>
                  <td>{element.quantity}</td>
                  <td>
                    <button
                      className="shield"
                      onClick={event => {
                        this.addStock(element.quantity + 1, element.id);
                      }}
                    >
                      <FontAwesomeIcon icon={faPlus} />
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default Admin;
