import React, { Component } from "react";
import "../css/App.css";
import { Navbar, Nav } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import { LinkContainer } from "react-router-bootstrap";
import Routes from "../routes/Routes";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faShoppingCart,
  faUser,
  faSignOutAlt
} from "@fortawesome/free-solid-svg-icons";
import { getBasedItems } from "../actions/getBasedItems";
import Cookies from "universal-cookie";
import { keepShoppingCart } from "../actions/keppShoppingCart";

class App extends Component {
  getData = () => {
    fetch("http://localhost:9000/product")
      .then(res => res.json())
      .then(res => this.props.getBasedItems(res))
      .catch(error => console.error("Error:", error));
  };

  componentWillMount() {
    this.getData();
    const cookies = new Cookies();
    const panier = cookies.get("panier");
    this.props.keepShoppingCart(panier);
  }

  render() {
    const cookies = new Cookies();
    const panier = cookies.get("panier");
    return (
      <div className="App">
        <Navbar bg="dark" variant="dark" className="Navbar">
          <nav className="NavMargin">
            <LinkContainer to="/">
              <Navbar.Brand>Home</Navbar.Brand>
            </LinkContainer>
          </nav>
          <Nav className="NavMargin">
            <LinkContainer to="/LayoutItems/ALL">
              <Nav.Link>Sells</Nav.Link>
            </LinkContainer>
          </Nav>
          {cookies.get("User") !== undefined &&
            cookies.get("User").Data.userTypeID === 1 && (
              <Nav className="NavMargin">
                <LinkContainer to="/Admin">
                  <Nav.Link>Admin</Nav.Link>
                </LinkContainer>
              </Nav>
            )}

          <Nav className="mr-auto navMargin">
            <LinkContainer to="/Contact">
              <Nav.Link>Contact</Nav.Link>
            </LinkContainer>
          </Nav>
          <Nav className="NavMargin">
            <LinkContainer to="/ShoppingCart">
              <Nav.Link>
                Panier
                <span className="badge badge-dark badge-pill">
                  {panier.length}
                </span>
                <FontAwesomeIcon icon={faShoppingCart} />
              </Nav.Link>
            </LinkContainer>
          </Nav>
          <Nav className="NavMargin">
            {cookies.get("User") === undefined && (
              <LinkContainer to="/Login">
                <Nav.Link>
                  Login <FontAwesomeIcon icon={faUser} />
                </Nav.Link>
              </LinkContainer>
            )}
          </Nav>
          <Nav className="NavMargin" />
          {cookies.get("User") !== undefined && (
            <a
              href="#"
              onClick={event => {
                cookies.remove("User");
                document.location.reload(true);
              }}
            >
              Deconnexion <FontAwesomeIcon icon={faSignOutAlt} />
            </a>
          )}
          <Nav />
        </Navbar>
        <Routes />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    shoppingCartElements: state.shoppingCartElements,
    dataBasedItems: state.dataBasedItems
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getBasedItems: newItems => {
      dispatch(getBasedItems(newItems));
    },
    keepShoppingCart: shoppingCart => {
      dispatch(keepShoppingCart(shoppingCart));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
