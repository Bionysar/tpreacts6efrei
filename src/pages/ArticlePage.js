import React, { Component } from "react";
import "../css/ArticlePage.css";
import "bootstrap/dist/css/bootstrap.css";
import NavBarCat from "../components/NavBarCat";
import { connect } from "react-redux";
import { addArticle } from "../actions/addArticle";
class ArticlePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      productCharac: []
    };
  }
  handleClick = newArticle => {
    this.props.addArticle(newArticle);
  };
  getData = () => {
    fetch("http://localhost:9000/productinfos", {
      method: "POST",
      body: JSON.stringify({
        Data: { id: this.props.match.params.id }
      }),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(res => this.setState({ productCharac: res }))
      .catch(error => console.error("Error:", error));
  };

  componentWillMount() {
    this.getData();
  }

  render() {
    return (
      <div>
        <NavBarCat />

        {this.props.dataBasedItems
          .filter(
            element =>
              element.id.toString() === this.props.match.params.id.toString()
          )
          .map((element, index) => (
            <div key={index}>
              <h1>{element.name}</h1>
              <img
                src={element.picture}
                className="img-resize"
                alt={element.picture}
              />

              <div className="ajout-panier">
                <h2>{element.price} €</h2>
                <a
                  href="#"
                  className="btn btn-primary"
                  onClick={() =>
                    this.props.addArticle({
                      id: element.id,
                      picture: element.picture,
                      name: element.name,
                      quantity: 1,
                      price: element.price,
                      alt: element.alt
                    })
                  }
                >
                  Mettre dans le panier
                </a>
                <p />
              </div>
            </div>
          ))}
        <table className="table table-striped">
          <thead>
            <tr>
              <th colSpan="2">Charactéristiques du produit</th>
            </tr>
            <tr>
              <th scope="col">Type</th>
              <th scope="col">Description</th>
            </tr>
          </thead>
          {this.state.productCharac.map((element, index) => (
            <tbody key={index}>
              <tr>
                <td>{element.title} </td>
                <td>{element.description}</td>
              </tr>
            </tbody>
          ))}
        </table>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    dataBasedItems: state.dataBasedItems
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addArticle: newArticle => {
      dispatch(addArticle(newArticle));
    }
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ArticlePage);
