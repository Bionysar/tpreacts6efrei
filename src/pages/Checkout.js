import React, { Component } from "react";
import "../css/Checkout.css";
import "bootstrap/dist/css/bootstrap.css";
import Cookies from "universal-cookie";

export default class Checkout extends Component {
  constructor(props) {
    super(props);
    const cookies = new Cookies();
    if (cookies.get("User") === undefined) {
      this.state = {
        userid: "",
        nom: "",
        prenom: "",
        adresse: "",
        ville: "",
        cp: "",
        cardnumber: "",
        cardname: "",
        expidate: "",
        cvv: "",
        apiResponse: "",
        erreur: ""
      };
      this.props.history.push("/Login");
    } else {
      this.state = {
        userid: cookies.get("User").Data.userID,
        nom: "",
        prenom: "",
        adresse: "",
        ville: "",
        cp: "",
        cardnumber: "",
        cardname: "",
        expidate: "",
        cvv: "",
        apiResponse: "",
        erreur: ""
      };
    }
  }
  changeFormValue = () => {
    if (this.state.apiResponse === "succes") {
      this.props.history.push("/FinCommande");
    } else {
      return <h1>NON</h1>;
    }
  };

  handleSubmit = event => {
    event.preventDefault();
    const cookies = new Cookies();
    const panier = cookies.get("panier");
    if (panier.length !== 0) {
      fetch("http://localhost:9000/order", {
        method: "POST",
        body: JSON.stringify({
          Data: {
            adresse: this.state.adresse,
            cp: this.state.cp,
            ville: this.state.ville,
            userid: this.state.userid,
            products: panier
          }
        }),
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(res => res.text())
        .then(res => this.setState({ apiResponse: res }))
        .then(res => this.changeFormValue());
    } else {
      this.setState({
        erreur: "Votre panier est vide, la commande n'a pas pu etre effectuée"
      });
    }
  };

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  render() {
    return (
      <div className="container" id="checkout">
        <form onSubmit={this.handleSubmit}>
          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="inputEmail4">Nom</label>
              <input
                type="text"
                className="form-control"
                id="inputNom"
                placeholder="Nom"
                name="nom"
                value={this.state.nom}
                onChange={this.handleChange}
                required
              />
            </div>
            <div className="form-group col-md-6">
              <label htmlFor="inputPassword4">Prenom</label>
              <input
                type="text"
                className="form-control"
                id="inputPrenom"
                placeholder="Prenom"
                name="prenom"
                value={this.state.prenom}
                onChange={this.handleChange}
                required
              />
            </div>
          </div>
          <div className="form-group">
            <label htmlFor="inputAddress">Adresse</label>
            <input
              type="text"
              className="form-control"
              id="inputAddress"
              placeholder="12 rue des alléas ..."
              name="adresse"
              value={this.state.adresse}
              onChange={this.handleChange}
              required
            />
          </div>
          <div className="form-row">
            <div className="form-group col-md-8">
              <label htmlFor="inputCity">Ville</label>
              <input
                type="text"
                className="form-control"
                id="inputCity"
                placeholder="Paris..."
                name="ville"
                value={this.state.ville}
                onChange={this.handleChange}
                required
              />
            </div>
            <div className="form-group col-md-4">
              <label htmlFor="inputZip">Code postal</label>
              <input
                type="text"
                className="form-control"
                id="inputZip"
                placeholder="92000..."
                name="cp"
                value={this.state.cp}
                onChange={this.handleChange}
                required
              />
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-4">
              <label htmlFor="inputEmail4">Card number</label>
              <input
                type="number"
                className="form-control"
                id="inputCardNumber"
                placeholder="2234..."
                name="cardnumber"
                value={this.state.cardnumber}
                onChange={this.handleChange}
                required
              />
            </div>
            <div className="form-group col-md-3">
              <label htmlFor="inputPassword4">Name on card</label>
              <input
                type="text"
                className="form-control"
                id="inputCardName"
                placeholder="Nom-Prenom"
                name="cardname"
                value={this.state.cardname}
                onChange={this.handleChange}
                required
              />
            </div>
            <div className="form-group col-md-3">
              <label htmlFor="inputPassword4">Expiration date</label>
              <input
                type="date"
                className="form-control"
                id="inputExpirationDate"
                name="expidate"
                value={this.state.expidate}
                onChange={this.handleChange}
                required
              />
            </div>
            <div className="form-group col-md-2">
              <label htmlFor="inputPassword4">CVV</label>
              <input
                type="text"
                className="form-control"
                id="inputCVV"
                placeholder="XXX"
                name="cvv"
                value={this.state.cvv}
                onChange={this.handleChange}
                required
              />
            </div>
          </div>
          <button type="submit" className="btn btn-primary">
            Commander
          </button>
          <h1 style={{ color: "red" }}>{this.state.erreur}</h1>
        </form>
      </div>
    );
  }
}
