import React, { Component } from "react";
import "../css/Contact.css";
import "bootstrap/dist/css/bootstrap.css";

class Contact extends Component {
  render() {
    return (
      <div className="testCard test">
        <div className="box">
          <div className="card">
            <div className="imgBx">
              <img src="https://imgur.com/KfzWB0V.jpg" alt="images" />
            </div>
            <div className="details">
              <h2>
                Steven
                <br />
                <span>Le mec</span>
              </h2>
            </div>
          </div>
          <div className="card">
            <div className="imgBx">
              <img src="https://i.imgur.com/VoQjlhO.png" alt="images" />
            </div>
            <div className="details">
              <h2>
                Rémi
                <br />
                <span>Le type</span>
              </h2>
            </div>
          </div>

          <div className="card">
            <div className="imgBx">
              <img src="https://i.imgur.com/a3FWVfr.png" alt="images" />
            </div>
            <div className="details">
              <h2>
                Laurent
                <br />
                <span>L'humain</span>
              </h2>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Contact;
