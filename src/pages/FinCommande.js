import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.css";
import { LinkContainer } from "react-router-bootstrap";
import { Nav } from "react-bootstrap";
import Cookies from "universal-cookie";

class FinCommande extends Component {
  componentWillMount() {
    const cookies = new Cookies();
    cookies.set("panier", [], { path: "/" });
  }
  render() {
    return (
      <div className="ShoppingCart">
        <section className="jumbotron text-center">
          <div className="container">
            <h1 className="jumbotron-heading">Commande réalisée avec succès</h1>
          </div>
        </section>
        <div className="container mb-4">
          <div className="row">
            <div className="col mb-2">
              <div className="row">
                <div className="col-sm-12  col-md-12">
                  <LinkContainer to="/" className="btn btn-block btn-light">
                    <Nav.Link>Retour a l'accueil</Nav.Link>
                  </LinkContainer>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default FinCommande;
