import React, { Component } from "react";
import "../css/Home.css";
import "bootstrap/dist/css/bootstrap.css";
import { Carousel } from "react-bootstrap";
import { Navbar, Nav } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      apiResponse: ""
    };
  }
  render() {
    return (
      <div className="Home">
        <Carousel>
          <Carousel.Item>
            <LinkContainer to="/LayoutItems/ALL">
              <Nav.Link className="link1">
                <img
                  className="carrouselPicture picture1"
                  src="https://i.imgur.com/zO894Og.jpg"
                  alt="First slide"
                />
              </Nav.Link>
            </LinkContainer>

            <Carousel.Caption className="caption1">
              <h3>Top Materiel achat</h3>
              <p>
                Vous cherchez quelque chose ? nous l'avons forcement, lancez
                vous !
              </p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="carrouselPicture"
              src="https://i.imgur.com/JZaKLe0.png"
              alt="Third slide"
            />
            <Carousel.Caption>
              <h3>Top Materiel achat</h3>
              <p>
                Vous cherchez quelque chose ? nous l'avons forcement, lancez
                vous !
              </p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="carrouselPicture"
              src="https://i.imgur.com/meiSgiR.png"
              alt="Third slide"
            />
            <Carousel.Caption>
              <h3>Top Materiel achat</h3>
              <p>
                Vous cherchez quelque chose ? nous l'avons forcement, lancez
                vous !
              </p>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>
      </div>
    );
  }
}

export default Home;
