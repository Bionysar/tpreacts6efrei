import React, { Component } from "react";
import "../css/Login.css";
import logo from "../images/logo.svg";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import "bootstrap/dist/css/bootstrap.css";
import { LinkContainer } from "react-router-bootstrap";
import { Nav } from "react-bootstrap";
import Cookies from "universal-cookie";

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      apiResponse: "undefineapi",
      productCharac: []
    };
  }

  validateForm() {
    return this.state.email.length > 0 && this.state.password.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  changeFormValue = res => {
    if (true) {
      console.log("TEST" + res);
      const cookies = new Cookies();
      cookies.set("login", this.state.email, { path: "/" });
      cookies.set("pass", this.state.password, { path: "/" });
      /*
      cookies.set("UserID:", this.state.productCharac.Data.userID, {
        path: "/"
      });
      */
      console.log("refidrection");
      console.log(cookies.get("login"));
      this.props.history.push("/");
    } else {
      console.log("NON");
    }
  };
  handleSubmit = event => {
    event.preventDefault();
    fetch("http://localhost:9000/login", {
      method: "POST",
      body: JSON.stringify({
        Data: { login: this.state.email, pass: this.state.password }
      }),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.text())
      .then(res => {
        console.log("TEST" + res);
        const cookies = new Cookies();
        cookies.set("User", res, { path: "/" });
        document.location.reload(true);
      })
      .catch(error => console.error("Error:", error));
  };

  render() {
    const cookies = new Cookies();
    if (cookies.get("User") !== undefined) {
      this.props.history.push("/");
    }
    return (
      <div className="Login">
        <img src={logo} className="Login-logo" alt="logo" />
        <Form onSubmit={this.handleSubmit}>
          <Form.Group controlId="email">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              autoFocus
              type="email"
              value={this.state.email}
              onChange={this.handleChange}
            />
          </Form.Group>
          <Form.Group controlId="password">
            <Form.Label>Password</Form.Label>
            <Form.Control
              value={this.state.password}
              onChange={this.handleChange}
              type="password"
            />
          </Form.Group>
          <Button
            block
            variant="primary"
            disabled={!this.validateForm()}
            type="submit"
          >
            Login
          </Button>
          <LinkContainer to="/Register">
            <Nav.Link>Cliquer ici pour vous inscrire !</Nav.Link>
          </LinkContainer>
        </Form>
      </div>
    );
  }
}
