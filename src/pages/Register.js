import React, { Component } from "react";
import "../css/Login.css";
import logo from "../images/logo.svg";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import "bootstrap/dist/css/bootstrap.css";

export default class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "a@a.com",
      password: "undefinedpass",
      datebirth: "",
      apiResponse: "undefineapi"
    };
  }

  validateForm() {
    return this.state.email.length > 0 && this.state.password.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  changeFormValue = () => {
    if (this.state.apiResponse === "Inscription réussi") {
      console.log("OUI");
      this.props.history.push("/Login");
    } else {
      console.log("NON");
    }
  };

  handleSubmit = event => {
    event.preventDefault();
    fetch("http://localhost:9000/register", {
      method: "POST",
      body: JSON.stringify({
        Data: {
          login: this.state.email,
          pass: this.state.password,
          datebirth: this.state.datebirth
        }
      }),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.text())
      .then(res => this.setState({ apiResponse: res }))
      .then(res => this.changeFormValue())
      .catch(error => console.error("Error:", error));
  };

  render() {
    return (
      <div className="Login">
        <img src={logo} className="Login-logo" alt="logo" />
        <p className="App-intro">{this.state.apiResponse}</p>
        <Form onSubmit={this.handleSubmit}>
          <Form.Group controlId="email">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              autoFocus
              type="email"
              value={this.state.email}
              onChange={this.handleChange}
            />
          </Form.Group>
          <Form.Group controlId="password">
            <Form.Label>Password</Form.Label>
            <Form.Control
              value={this.state.password}
              onChange={this.handleChange}
              type="password"
            />
          </Form.Group>
          <Form.Group controlId="datebirth">
            <Form.Label>Date d'anniversaire</Form.Label>
            <Form.Control
              autoFocus
              type="date"
              value={this.state.datebirth}
              onChange={this.handleChange}
            />
          </Form.Group>
          <Button
            block
            variant="primary"
            disabled={!this.validateForm()}
            type="submit"
          >
            Register
          </Button>
        </Form>
      </div>
    );
  }
}
