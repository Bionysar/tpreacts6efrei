import React, { Component } from "react";
import ShoppingCartElement from "../components/ShoppingCartElement";
import "bootstrap/dist/css/bootstrap.css";
import { LinkContainer } from "react-router-bootstrap";
import { Nav } from "react-bootstrap";
import { connect } from "react-redux";
import { deleteArticle } from "../actions/deleteArticle";
import Cookies from "universal-cookie";

class ShoppingCart extends Component {
  handleShoppingElementClick = id => {
    this.props.deleteArticle(id);
  };
  render() {
    const cookies = new Cookies();
    const panier = cookies.get("panier");

    const price = panier.reduce(function(cnt, o) {
      return cnt + o.price;
    }, 0);
    const shipping = panier.reduce(function(cnt, o) {
      return cnt + 2.5;
    }, 0);
    return (
      <div className="ShoppingCart">
        <section className="jumbotron text-center">
          <div className="container">
            <h1 className="jumbotron-heading">PANIER DE TOP MATERIEL ACHAT</h1>
          </div>
        </section>

        <div className="container mb-4">
          <div className="row">
            <div className="col-12">
              <div className="table-responsive">
                <table className="table table-striped">
                  <thead>
                    <tr>
                      <th scope="col"> </th>
                      <th scope="col">Product</th>
                      <th scope="col">Available</th>
                      <th scope="col" className="text-center">
                        Quantity
                      </th>
                      <th scope="col" className="text-right">
                        Price
                      </th>
                      <th> </th>
                    </tr>
                  </thead>
                  <tbody>
                    {panier.map((element, index) => (
                      <ShoppingCartElement
                        id={element.id}
                        picture={element.picture}
                        name={element.name}
                        quantity={element.quantity}
                        price={element.price}
                        index={index}
                        onClick={this.handleShoppingElementClick}
                        alt={element.alt}
                        key={index}
                      />
                    ))}

                    <tr>
                      <td />
                      <td />
                      <td />
                      <td />
                      <td>Sub-Total</td>
                      <td className="text-right">{price}€</td>
                    </tr>
                    <tr>
                      <td />
                      <td />
                      <td />
                      <td />
                      <td>Shipping</td>
                      <td className="text-right">{shipping} €</td>
                    </tr>
                    <tr>
                      <td />
                      <td />
                      <td />
                      <td />
                      <td>
                        <strong>Total</strong>
                      </td>
                      <td className="text-right">
                        <strong>{price + shipping} €</strong>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className="col mb-2">
              <div className="row">
                <div className="col-sm-12  col-md-6">
                  <LinkContainer to="/" className="btn btn-block btn-light">
                    <Nav.Link>Continuer le shopping</Nav.Link>
                  </LinkContainer>
                </div>
                <div className="col-sm-12 col-md-6 text-right">
                  <LinkContainer
                    to="/Checkout"
                    className="btn btn-lg btn-block btn-success text-uppercase"
                  >
                    <Nav.Link>Panier</Nav.Link>
                  </LinkContainer>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    shoppingCartElements: state.shoppingCartElements
  };
};

const mapDispatchToProps = dispatch => {
  return {
    deleteArticle: id => {
      dispatch(deleteArticle(id));
    }
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShoppingCart);
