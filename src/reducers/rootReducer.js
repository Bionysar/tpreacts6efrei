import Cookies from "universal-cookie";
const initState = {
  shoppingCartElements: [],
  dataBasedItems: []
};
const cookies = new Cookies();
const rootReducer = (state = initState, action) => {
  const panier = cookies.get("panier");
  switch (action.type) {
    case "DELETE_ARTICLE":
      let updatedShoppingCart = state.shoppingCartElements.filter(item => {
        return action.id !== item.id;
      });
      cookies.set("panier", updatedShoppingCart);
      return {
        ...state,
        shoppingCartElements: updatedShoppingCart
      };
    case "ADD_ARTICLE":
      var panFiltrer = panier.filter(
        element => element.id.toString() === action.newArticle.id.toString()
      );
      var panNonFiltrer = panier.filter(
        element => element.id.toString() !== action.newArticle.id.toString()
      );
      if (panFiltrer.length === 1) {
        var maxQuantity = state.dataBasedItems.filter(
          element => element.id.toString() === panFiltrer[0].id.toString()
        )[0].quantity;
        if (panFiltrer[0].quantity < maxQuantity) {
          panFiltrer[0].quantity = panFiltrer[0].quantity + 1;
        }

        panNonFiltrer.push(panFiltrer[0]);
        cookies.set("panier", panNonFiltrer, { path: "/" });
        return {
          ...state,
          shoppingCartElements: panNonFiltrer
        };
      } else {
        cookies.set("panier", [...panier, action.newArticle], { path: "/" });
        return {
          ...state,
          shoppingCartElements: [...panier, action.newArticle]
        };
      }

    case "GET_ITEMS":
      return {
        ...state,
        dataBasedItems: action.newItems
      };
    case "KEEP_ITEMS":
      return {
        ...state,
        shoppingCartElements: action.shoppingCart
      };
    default:
      return state;
  }
};

export default rootReducer;
