import React from "react";
import { Route, Switch } from "react-router-dom";
import Login from "../pages/Login";
import ShoppingCart from "../pages/ShoppingCart";
import Checkout from "../pages/Checkout";
import Home from "../pages/Home";
import NotFound from "../pages/NotFound";
import Sells from "../pages/Sells";
import Register from "../pages/Register";
import ArticlePage from "../pages/ArticlePage";
import NavBarCat from "../components/NavBarCat";
import Contact from "../pages/Contact";
import LayoutItems from "../components/LayoutItems";
import FinCommande from "../pages/FinCommande";
import Admin from "../pages/Admin";
//cimport DisplaySells from "../components/DisplaySells";

export default () => (
  <Switch>
    <Route path="/" exact component={Home} />
    <Route path="/Login" exact component={Login} />
    <Route path="/ShoppingCart" exact component={ShoppingCart} />
    <Route path="/Checkout" exact component={Checkout} />
    <Route path="/Sells" exact component={Sells} />
    <Route path="/NavBarCat" exact component={NavBarCat} />
    <Route path="/Contact" exact component={Contact} />
    <Route path="/LayoutItems/:category" exact component={LayoutItems} />
    <Route path="/Register" exact component={Register} />
    <Route path="/ArticlePage/:id" exact component={ArticlePage} />
    <Route path="/FinCommande" exact component={FinCommande} />
    <Route path="/Admin" exact component={Admin} />
    {/* catch all unmatched routes and redirect them to 404 not found page*/}
    <Route component={NotFound} />
  </Switch>
);
